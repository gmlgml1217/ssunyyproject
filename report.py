# -*- coding: utf-8 -*-
import re
import urllib.request

from bs4 import BeautifulSoup

from flask import Flask
from slack import WebClient
from slackeventsapi import SlackEventAdapter

from slack.web.classes import extract_json
from slack.web.classes.blocks import *


SLACK_TOKEN = 'xoxb-691872831238-689260720068-YdG81SQ81FwH5pL34FcVOyXB'
SLACK_SIGNING_SECRET = '3ecc689192da6c53fea879e2453f3469'


app = Flask(__name__)
# /listening 으로 슬랙 이벤트를 받습니다.
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)


# 크롤링 함수 구현하기
def _crawl_music_chart(text):
    url = 'http://www.ikaraoke.kr/isong/hit_song.asp?page=1&week_page='
    sourcecode = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(sourcecode, "html.parser")
    
    url_2 = 'http://www.ikaraoke.kr/isong/hit_song_monthly.asp?page=1&month_page='
    sourcecode = urllib.request.urlopen(url_2).read()
    soup_2 = BeautifulSoup(sourcecode, "html.parser")

    titles = []
    titles_2 = []
    artists = []
    numbers = []

    titles_m = []
    artists_m = []
    numbers_m = []

    # 곡번호 뽑기
    body = soup.find_all(class_="ac")
    compile_text = re.compile("<em>\d\d\d+")
    numbers = compile_text.findall(str(body))
    cnt = 1

    for i in range(len(numbers)):
        numbers[i] = numbers[i].strip("<em>")

    ###################################
    body = soup_2.find_all(class_="ac")
    compile_text = re.compile("<em>\d\d\d+")
    numbers_m = compile_text.findall(str(body))
    cnt = 1

    for i in range(len(numbers_m)):
        numbers_m[i] = numbers_m[i].strip("<em>")

    # 곡제목 뽑기
    body = soup.find_all('td', class_="pl8")
    compile_text = re.compile("title='[\S\s]+'")

    for title in soup.find_all('td', class_="pl8"):
        if cnt%3 == 1:
            title = compile_text.findall((str(title)).replace('"', "'"))
            titles.append(str(title).split("'")[1])
            titles_2.append(str(title).split("'")[1].replace(" ", "").replace(",", ""))
        cnt += 1

    ###################################
    body = soup_2.find_all('td', class_="pl8")
    compile_text = re.compile("title='[\S\s]+'")
    
    cnt = 1
    for title_2 in soup_2.find_all('td', class_="pl8"):
        if cnt%3 == 1:
            title_2 = compile_text.findall((str(title_2)).replace('"', "'"))
            titles_m.append(str(title_2).split("'")[1])
        cnt += 1
    
    result = []

    # 아티스트 뽑기
    for artist in soup.find_all(class_="tit pl8"):
        artists.append(artist.get_text().strip())

    ###################################
    for artist in soup_2.find_all(class_="tit pl8"):
        artists_m.append(artist.get_text().strip())
    
    text = text.split(">")[1].replace(" ", "").replace(",", "")

    if not text:
        return "*♬아래와 같이 검색해주세요♬*\n`노래제목` `주간순위` `월간순위`" 

    elif "주간순위" in text:
        for h in soup.find_all(class_="ranking"):
            result.append("[ *"+h.get_text().split("\n")[1]+"* ]")
        
        for i in range(50):
            if(i<9):
                result.append("0"+str(i+1)+"위 : "+ titles[i]+" - "+artists[i]+" - "+numbers[i])
                #print("0"+str(i+1)+"위 : "+ titles[i]+" - "+artists[i])
            else:
                result.append(str(i+1)+"위 : "+ titles[i]+" - "+artists[i]+" - "+numbers[i])
        
        return '\n'.join(result)

    elif "월간순위" in text:
        for h in soup_2.find_all(class_="ranking"):
            result.append("[ *"+h.get_text().split("\n")[1]+"* ]")
        for i in range(50):
            if(i<9):
                result.append("0"+str(i+1)+"위 : "+ titles_m[i]+" - "+artists_m[i]+" - "+numbers_m[i])
            else:
                result.append(str(i+1)+"위 : "+ titles_m[i]+" - "+artists_m[i]+" - "+numbers_m[i])
        
        return '\n'.join(result)
    
    else:
        for i in range(len(titles_2)):
            if titles_2[i].startswith(text):
                result.append(numbers[i] + " - " + titles[i] + " - " + artists[i])

        if not result:
            return "*♬아래와 같이 검색해주세요♬*\n`노래제목` `주간순위` `월간순위`" 
    
    return '\n'.join(result)


# 챗봇이 멘션을 받았을 경우
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
    channel = event_data["event"]["channel"]
    text = event_data["event"]["text"]

    message = _crawl_music_chart(text)
    slack_web_client.chat_postMessage(
        channel=channel,
        text = message
    )


# / 로 접속하면 서버가 준비되었다고 알려줍니다.
@app.route("/", methods=["GET"])
def index():
    return "<h1>Server is ready.</h1>"


if __name__ == '__main__':
    app.run('0.0.0.0', port=5000)